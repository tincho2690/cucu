// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
   // Initialize Firebase
   firebase: {
    apiKey: "AIzaSyDoeHUbE2GFdGe1W0mV0mnJZGdAf4xEhIY",
    authDomain: "cucu-6547a.firebaseapp.com",
    databaseURL: "https://cucu-6547a.firebaseio.com",
    projectId: "cucu-6547a",
    storageBucket: "cucu-6547a.appspot.com",
    messagingSenderId: "158145273447"
  }
}