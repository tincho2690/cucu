import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';

@Injectable()
export class DataService {

  constructor(private db: AngularFireDatabase) { 
  
  
  // db.database().ref().child('participante');
  }
  
  getParticipantesWithKeys(){
    
    let participantes: Array<any> = [];
    this.db.object('participante').snapshotChanges().map(action =>{ 
      const data = action.payload.toJSON(); 
      return data; }).subscribe(result =>{ 
        Object.keys(result).map(key=>{ participantes.push({ 
          'key': key, 'data':result[key] 
        }); 
      }); 
    });
    return participantes;
  
  }

  getParticipantes(){
    return this.db.list('/participante');
  }
  
  getParticipanteById(idParticipante){
    return this.db.object('/participante/'+idParticipante).valueChanges();
    
    // this.cars = this.afo.database.list('/issues/10/users/1/cars') // car user data
    // .map(carsMeta => {
    //   return carsMeta.map(carMeta => {
    //     this.afo.database.object(`/issues/10/cars/${carMeta.id}`) // car data
    //       .subscribe(carData => carMeta.data = carData); // merge car data to meta object
    //     return carMeta; // return merged car data
    // return this.db.object('/participante/'+idParticipante).valueChanges();
  
  }

  createParticipante(participante){
   let participantes = this.getParticipantes()
   participantes.push(participante);

   }

  getLocaciones(){
    return this.db.list('/locacion')
  }

  getLocacionById(idLocacion){
    return this.db.object('/locacion'+idLocacion).valueChanges();
  }

  getLocacionesWithKey(){

    let locaciones: Array<any> = [];
    this.db.object('locacion').snapshotChanges().map(action =>{ 
      const data = action.payload.toJSON(); 
      return data; }).subscribe(result =>{ 
        Object.keys(result).map(key=>{ locaciones.push({ 
          'key': key, 'data':result[key] 
        }); 
      }); 
    });
    console.log(locaciones)
    return locaciones;
  }

  deleteParticipante(idParticipante){
    this.db.object('/participante/' + idParticipante).remove();
  }
}
