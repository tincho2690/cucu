import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantesBusquedaComponent } from './participantes-busqueda.component';

describe('ParticipantesBusquedaComponent', () => {
  let component: ParticipantesBusquedaComponent;
  let fixture: ComponentFixture<ParticipantesBusquedaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantesBusquedaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantesBusquedaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
