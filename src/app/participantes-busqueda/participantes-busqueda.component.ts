import { AngularFireDatabase } from 'angularfire2/database';
import { DataService } from './../Services/dataService/data-service.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs/Subscription';


@Component({
  selector: 'participantes-busqueda',
  templateUrl: './participantes-busqueda.component.html',
  styleUrls: ['./participantes-busqueda.component.css']
})
export class ParticipantesBusquedaComponent implements OnInit {
  participantesPrueba;
  locacionPrueba;
  participantes: Array<any> = [];
  
  constructor(private db:AngularFireDatabase,private ds:DataService, private router:Router) {
    
    // this.locacionPrueba = this.db.database.ref().child('locacion');
    // this.participantesPrueba = this.db.database.ref().child('participante');

    // this.participantesPrueba.on('child_added',function(snap){
    //   console.log(snap.val());
    //   this.locacionPrueba.child(snap.val().idClub).once('value',club =>{
    //     console.log(club.val());
    //   })
    // })


    this.participantes = ds.getParticipantesWithKeys();
  }
   
  ngOnInit() {
  }
  
  onClick(idParticipante){
    this.router.navigate(['/participantes/'+idParticipante]);
  }

}
