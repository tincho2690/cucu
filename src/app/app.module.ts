import { DataService } from './Services/dataService/data-service.service';
import { environment } from './../environments/environment';
import { ParticipantesDisplayComponent } from './participantes-display/participantes-display.component';
import { ParticipantesBusquedaComponent } from './participantes-busqueda/participantes-busqueda.component';
import { ParticipantesAltaComponent } from './participantes-alta/participantes-alta.component';
import { EventosAltaComponent } from './eventos-alta/eventos-alta.component';
import { EventosDisplayComponent } from './eventos-display/eventos-display.component';
import { EventosBusquedaComponent } from './eventos-busqueda/eventos-busqueda.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';

import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { NgDatepickerModule } from 'ng2-datepicker';

import { ProximosEventosComponent } from './proximos-eventos/proximos-eventos.component';
import { HomeNavbarComponent } from './home-navbar/home-navbar.component';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  declarations: [
    AppComponent,
    ProximosEventosComponent,
    HomeNavbarComponent,
    EventosBusquedaComponent,
    EventosDisplayComponent,
    EventosAltaComponent,
    ParticipantesAltaComponent,
    ParticipantesBusquedaComponent,
    ParticipantesDisplayComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    NgDatepickerModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatInputModule,

    MatNativeDateModule,

    RouterModule.forRoot([
      {
        path:'proximosEventos', 
        component:ProximosEventosComponent
      },
      {
        path:'participantes', 
        component:ParticipantesBusquedaComponent
      },
      {
        path:'participantes/nuevo', 
        component:ParticipantesAltaComponent
      },
      {
        path:'participantes/:idParticipante',
        component: ParticipantesDisplayComponent
      },
      {
        path:'eventos', 
        component:EventosBusquedaComponent
      },
      {
        path:'**', 
        component:ProximosEventosComponent
      }
    ])
  ],
  providers: [    
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
