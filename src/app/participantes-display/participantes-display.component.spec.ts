import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantesDisplayComponent } from './participantes-display.component';

describe('ParticipantesDisplayComponent', () => {
  let component: ParticipantesDisplayComponent;
  let fixture: ComponentFixture<ParticipantesDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantesDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantesDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
