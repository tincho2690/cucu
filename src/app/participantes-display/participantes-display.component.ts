import { DataService } from './../Services/dataService/data-service.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-participantes-display',
  templateUrl: './participantes-display.component.html',
  styleUrls: ['./participantes-display.component.css']
})
export class ParticipantesDisplayComponent implements OnInit {
  
  idParticipante;
  participante$ ;
  locaciones:Array<any> = [];
  locacion$;
  participante:Observable<any[]>;

  constructor(private route: ActivatedRoute,
              private ds:DataService,
              private db: AngularFireDatabase,
              private router:Router){}

  ngOnInit() {
    
    this.idParticipante = this.route.snapshot.paramMap.get('idParticipante')
    this.participante$=this.ds.getParticipanteById(this.idParticipante).map;
    
    
    //console.log(this.participante$.idClub);
    // this.locaciones = this.ds.getLocacionById(idLocacion);
    // this.locacion$
    // console.log(this.locaciones.
    // console.log(this.locaciones.find(x => x.key == "1"))

  }

  eliminarParticipante(){
    this.ds.deleteParticipante(this.idParticipante);
    this.router.navigate(['/participantes']);
  }
}
