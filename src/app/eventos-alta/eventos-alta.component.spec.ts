import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventosAltaComponent } from './eventos-alta.component';

describe('EventosAltaComponent', () => {
  let component: EventosAltaComponent;
  let fixture: ComponentFixture<EventosAltaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventosAltaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventosAltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
