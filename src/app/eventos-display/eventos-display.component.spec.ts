import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventosDisplayComponent } from './eventos-display.component';

describe('EventosDisplayComponent', () => {
  let component: EventosDisplayComponent;
  let fixture: ComponentFixture<EventosDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventosDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventosDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
