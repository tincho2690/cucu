import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantesAltaComponent } from './participantes-alta.component';

describe('ParticipantesAltaComponent', () => {
  let component: ParticipantesAltaComponent;
  let fixture: ComponentFixture<ParticipantesAltaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantesAltaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantesAltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
