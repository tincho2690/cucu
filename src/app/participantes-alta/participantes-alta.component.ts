import { DataService } from './../Services/dataService/data-service.service';
import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import { DatepickerOptions } from 'ng2-datepicker';
import {Subscription} from 'rxjs/Subscription';
import { Router } from '@angular/router';

@Component({
  selector: 'participantes-alta',
  templateUrl: './participantes-alta.component.html',
  styleUrls: ['./participantes-alta.component.css']
})
export class ParticipantesAltaComponent implements OnInit {
  
  participantes: AngularFireList<any>;  
  clubs: Array<any> = [];;

    constructor(private ds:DataService, private router:Router) {
      
      this.participantes = ds.getParticipantes();
      this.clubs = ds.getLocacionesWithKey();

 }
  
  onSubmit(participante){
    
    participante.value.fechaNacimiento = this.dateConvertor(participante.value.fechaNacimiento);
    
    this.ds.createParticipante(participante.value);
    this.router.navigate(['/participantes'])
  }
  
  dateConvertor(date):string{
    return (date.getDate()+"/"+(date.getMonth() +1)+"/"+date.getFullYear())
  }
  
  ngOnInit() {
  }

}
